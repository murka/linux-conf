#!/bin/sh
set -e

#Sanity check
if [ ! -d "/sys/firmware/efi/efivars" ]; then
  echo "Error: System isn't booted in EFI mode"
  exit 1
fi

dirname() {
  a="/$1"
  a="${a%/*}"
  a="${a:-.}"
  a="${a#/}/"

  pushd "$a" >/dev/null
  echo "$(pwd)"
  popd >/dev/null
}

CURRENT_DIR="$(dirname $0)"
INITRD_SCRIPT_PATH="/etc/initramfs/post-update.d/z-copy-initrd-to-efi"
KERNEL_SCRIPT_PATH="/etc/kernel/postinst.d/z-copy-kernel-to-efi"

mkdir -p $(dirname "$KERNEL_SCRIPT_PATH")
echo "Copying '${CURRENT_DIR}/z-copy-kernel-to-efi' to '$KERNEL_SCRIPT_PATH'"
cp "${CURRENT_DIR}/z-copy-kernel-to-efi" "$KERNEL_SCRIPT_PATH"
chmod +x "$KERNEL_SCRIPT_PATH"

mkdir -p $(dirname "$INITRD_SCIPT_PATH")
echo "Copying '${CURRENT_DIR}/z-copy-initrd-to-efi' to '$INITRD_SCRIPT_PATH'"
cp "${CURRENT_DIR}/z-copy-initrd-to-efi" "$INITRD_SCRIPT_PATH"
chmod +x "$INITRD_SCRIPT_PATH"
