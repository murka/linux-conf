execute pathogen#infect()

let mapleader=","
filetype plugin indent on 

" Always show statusline
set laststatus=2

set t_Co=256
syntax on
let g:solarized_termtrans = 1
set background=dark
colorscheme solarized
set history=700

" Lines between cursor and edge to keep while scrolling
set so=7

set cursorline

set ignorecase
set hlsearch

set splitright
set splitbelow

" matching parenthesis
set showmatch

set nobackup

set number

" tab size 2
set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2

set smartindent
set autoindent

" Don't search from top when end hit
set nowrapscan

" linebreak at 500 chars
set lbr
set tw=500

" move virtual lines instead of real lines
map j gj
map k gk

"ctrl + _ to move windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" get permissions and write to file
cmap w!! w !sudo tee % > /dev/null

map <leader>r :source ~/.vimrc<CR>

" open nerdtree when vim opened on dir
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

map <C-n> :NERDTreeToggle<CR>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

let NERDTreeWinSize=40
